# build libs
#include $(all-subdir-makefiles)
LOCAL_PATH := $(call my-dir)

subdirs := $(addprefix $(LOCAL_PATH)/,$(addsuffix /Android.mk, \
		libavcodec \
		libavutil \
	))

include $(subdirs)
