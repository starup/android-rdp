#include <jni.h>
#include <fcntl.h>

#include "android_freerdp.hxx"
#include "android_graphics.h"

#define TAG "AndroidFreeRDP"
#include "jniUtils.h"

#ifdef ENABLE_LIBRARY_PROFILER
#include "../android-ndk-profiler-3.1/prof.h"
#endif

typedef struct android_context
{
	rdpContext _p;

	AndroidFreeRDP* androidFreeRDP;
	rdpSettings* settings;
} AndroidContext;

//-----------------------------------------------------------------------------

AndroidFreeRDP::AndroidFreeRDP()
	: mInstance(NULL), mListener(NULL)
	, mThread(0), mStopped(FALSE), mRunning(FALSE)
{    
	mInstance = freerdp_new();
	mInstance->PreConnect = android_pre_connect;
	mInstance->PostConnect = android_post_connect;
	mInstance->VerifyCertificate = android_verify_certificate;
	mInstance->ReceiveChannelData = android_receive_channel_data;

	mInstance->context_size = sizeof(AndroidContext);
	mInstance->ContextNew = android_context_new;
	mInstance->ContextFree = android_context_free;
	freerdp_context_new(mInstance);

	AndroidContext* context = (AndroidContext*) mInstance->context;
	context->androidFreeRDP = this;
	context->settings = mInstance->settings;
	
	//
	int flags;
    // 
    // For posix, we need to use a pipe to force unblock the select loop
    //
    pipe(mEventPipe);
    flags = fcntl(mEventPipe[0], F_GETFL,0);
    //
    // We need to set the pipe to nonblock, so we can blindly empty the pipe
    //
    fcntl(mEventPipe[0],F_SETFL,O_NONBLOCK | flags);
}

AndroidFreeRDP::~AndroidFreeRDP()
{	
	disconnect();
	
	rdpChannels* channels = mInstance->context->channels;
	freerdp_channels_free(channels);
	freerdp_context_free(mInstance);
	freerdp_free(mInstance);
	close(mEventPipe[0]);
	close(mEventPipe[1]);
}

//-----------------------------------------------------------------------------
void AndroidFreeRDP::android_context_new(freerdp* instance, rdpContext* context)
{
	LOGI("android_context_new");
	context->channels = freerdp_channels_new();
}

void AndroidFreeRDP::android_context_free(freerdp* instance, rdpContext* context)
{
	LOGI("android_context_free");
}

void AndroidFreeRDP::android_begin_paint(rdpContext* context)
{	
	rdpGdi* gdi = context->gdi;	
	
	//LOGI("android_begin_paint");
	
	gdi->primary->hdc->hwnd->invalid->null = 1;
}

void AndroidFreeRDP::android_end_paint(rdpContext* context)
{
    rdpGdi* gdi;
    AndroidFreeRDP* androidFreeRDP;
	INT32 x, y;
	INT32 w, h;

	//LOGI("android_end_paint");
	
    gdi = context->gdi;
    androidFreeRDP = ((AndroidContext *) context)->androidFreeRDP;

    if (gdi->primary->hdc->hwnd->invalid->null || !androidFreeRDP->mListener)
        return;
		
	x = gdi->primary->hdc->hwnd->invalid->x;
	y = gdi->primary->hdc->hwnd->invalid->y;
	w = gdi->primary->hdc->hwnd->invalid->w;
	h = gdi->primary->hdc->hwnd->invalid->h;
	androidFreeRDP->mListener->updateScreen(gdi, x, y, w, h);
}

BOOL AndroidFreeRDP::android_pre_connect(freerdp* instance)
{
	BOOL bitmap_cache;
	AndroidContext* context;
	rdpSettings* settings;

	LOGI("android_pre_connect");
	context = ((AndroidContext *) instance->context);
	settings = instance->settings;
	bitmap_cache = settings->BitmapCacheEnabled;

	settings->OrderSupport[NEG_DSTBLT_INDEX] = TRUE;
	settings->OrderSupport[NEG_PATBLT_INDEX] = TRUE;
	settings->OrderSupport[NEG_SCRBLT_INDEX] = TRUE;
	settings->OrderSupport[NEG_OPAQUE_RECT_INDEX] = TRUE;
	settings->OrderSupport[NEG_DRAWNINEGRID_INDEX] = FALSE;
	settings->OrderSupport[NEG_MULTIDSTBLT_INDEX] = FALSE;
	settings->OrderSupport[NEG_MULTIPATBLT_INDEX] = FALSE;
	settings->OrderSupport[NEG_MULTISCRBLT_INDEX] = FALSE;
	settings->OrderSupport[NEG_MULTIOPAQUERECT_INDEX] = TRUE;
	settings->OrderSupport[NEG_MULTI_DRAWNINEGRID_INDEX] = FALSE;
	settings->OrderSupport[NEG_LINETO_INDEX] = TRUE;
	settings->OrderSupport[NEG_POLYLINE_INDEX] = TRUE;
	settings->OrderSupport[NEG_MEMBLT_INDEX] = bitmap_cache;
	settings->OrderSupport[NEG_MEM3BLT_INDEX] = FALSE;
	settings->OrderSupport[NEG_MEMBLT_V2_INDEX] = bitmap_cache;
	settings->OrderSupport[NEG_MEM3BLT_V2_INDEX] = FALSE;
	settings->OrderSupport[NEG_SAVEBITMAP_INDEX] = FALSE;
	settings->OrderSupport[NEG_GLYPH_INDEX_INDEX] = FALSE;
	settings->OrderSupport[NEG_FAST_INDEX_INDEX] = FALSE;
	settings->OrderSupport[NEG_FAST_GLYPH_INDEX] = FALSE;
	settings->OrderSupport[NEG_POLYGON_SC_INDEX] = FALSE;
	settings->OrderSupport[NEG_POLYGON_CB_INDEX] = FALSE;
	settings->OrderSupport[NEG_ELLIPSE_SC_INDEX] = FALSE;
	settings->OrderSupport[NEG_ELLIPSE_CB_INDEX] = FALSE;	

	freerdp_channels_pre_connect(instance->context->channels, instance);

	return TRUE;
}

BOOL AndroidFreeRDP::android_post_connect(freerdp* instance)
{
	rdpGdi* gdi;
	AndroidContext* context;
	AndroidFreeRDP* thiz;
	RFX_CONTEXT* rfx_context = NULL;
	NSC_CONTEXT* nsc_context = NULL;	

	LOGI("android_post_connect");
	context = ((AndroidContext*) instance->context);
	thiz = context->androidFreeRDP;

	gdi_init(instance, /*CLRCONV_ALPHA | */CLRCONV_INVERT | CLRBUF_16BPP | CLRBUF_32BPP, NULL);
	gdi = instance->context->gdi;

	instance->update->BeginPaint = android_begin_paint;
	instance->update->EndPaint = android_end_paint;

	pointer_cache_register_callbacks(instance->update);
    android_register_graphics(instance->context->graphics);

	freerdp_channels_post_connect(instance->context->channels, instance);

	rfx_context = (RFX_CONTEXT*)gdi->rfx_context;
	nsc_context = (NSC_CONTEXT*)gdi->nsc_context;

       LOGI("rfx_context = %p, nsc_context = %p\n", rfx_context, nsc_context);
	if (rfx_context)
		rfx_context_set_cpu_opt(rfx_context, CPU_NEON);
	if (nsc_context)
		nsc_context_set_cpu_opt(nsc_context, CPU_NEON);

	if (thiz->mListener) {
		thiz->mListener->setDisplayMode(gdi->width, gdi->height, gdi->bytesPerPixel * 8);
	}

	return TRUE;
}

int AndroidFreeRDP::android_process_plugin_args(rdpSettings* settings, const char* name,
                                   RDP_PLUGIN_DATA* plugin_data, void* user_data)
{
	rdpChannels* channels = (rdpChannels*) user_data;

	LOGI("loading plugin %s\n", name);
	freerdp_channels_load_plugin(channels, settings, name, plugin_data);

	return 1;
}

BOOL AndroidFreeRDP::android_verify_certificate(freerdp* instance, char* subject, char* issuer, char* fingerprint)
{
	LOGI("Certificate details:\n");
	LOGI("\tSubject: %s\n", subject);
	LOGI("\tIssuer: %s\n", issuer);
	LOGI("\tThumbprint: %s\n", fingerprint);
	LOGI("The above X.509 certificate could not be verified, possibly because you do not have "
		"the CA certificate in your certificate store, or the certificate has expired. "
		"Please look at the documentation on how to create local certificate store for a private CA.\n");

    // TODO X509 Certificate verification
    return TRUE;
}

int AndroidFreeRDP::android_receive_channel_data(freerdp* instance, int channelId, UINT8* data, int size, int flags, int total_size)
{
	//LOGI("android_receive_channel_data");
	return freerdp_channels_data(instance, channelId, data, size, flags, total_size);
}

void AndroidFreeRDP::android_process_cb_monitor_ready_event(rdpChannels* channels, freerdp* instance)
{
	RDP_EVENT* event;
	RDP_CB_FORMAT_LIST_EVENT* format_list_event;

	//LOGI("android_process_cb_monitor_ready_event");
	event = freerdp_event_new(RDP_EVENT_CLASS_CLIPRDR, RDP_EVENT_TYPE_CB_FORMAT_LIST, NULL, NULL);

	format_list_event = (RDP_CB_FORMAT_LIST_EVENT*)event;
	format_list_event->num_formats = 0;

	freerdp_channels_send_event(channels, event);
}

void AndroidFreeRDP::android_process_channel_event(rdpChannels* channels, freerdp* instance)
{
    RDP_EVENT* event;

	//LOGI("android_process_channel_event");
    event = freerdp_channels_pop_event(channels);

    if (event) {
        switch (event->event_type) {
        case RDP_EVENT_TYPE_CB_MONITOR_READY:
            android_process_cb_monitor_ready_event(channels, instance);
            break;
        default:
            LOGI("android_process_channel_event: unknown event type %d\n", event->event_type);
            break;
        }

        freerdp_event_free(event);
    }
}

BOOL AndroidFreeRDP::android_get_fds(void** rfds, int* rcount, void** wfds, int* wcount)
{
	//LOGI("android_get_fds");
    rfds[*rcount] = (void *)(long)(mEventPipe[0]);
    (*rcount)++;

    return TRUE;
}

void AndroidFreeRDP::android_process_mouse_event(Android_Rdp_Mouse_Event_t *event)
{
	UINT16 device_flags = event->device_flags;
	int cursorx = event->x;
	int cursory = event->y;

    if (mInstance) {
        mInstance->input->MouseEvent(mInstance->input, device_flags,
                                   cursorx,
                                   cursory);
    }
}

void AndroidFreeRDP::android_process_key_event(Android_Rdp_Key_Event_t* event)
{
    UINT16 flags = 0;
    UINT16 uc = 0;
    RDP_SCANCODE scancode = 0;

    if (event->unicode) {
        uc = (UINT16)event->keycode;
    } else {
        scancode = freerdp_keyboard_get_rdp_scancode_from_virtual_key_code(event->keycode);
    }
    flags = event->device_flags;

    if (mInstance) {
#if 0
        LOGI("android_process_key_event: down %d scancode %d uc %d\n", 
                event->down, scancode, uc);
#endif
        if (event->unicode) {
            mInstance->input->UnicodeKeyboardEvent(mInstance->input, flags, uc);
        } else {
        	if (RDP_SCANCODE_EXTENDED(scancode)) {
        		flags |= KBD_FLAGS_EXTENDED;
        	}
            mInstance->input->KeyboardEvent(mInstance->input, flags, RDP_SCANCODE_CODE(scancode));
        }
    }
}

void AndroidFreeRDP::android_process_touch_event(Android_Rdp_Touch_Event_t* event, int count)
{
    RDP_DRDYNVC_INPUT_EVENT* e = (RDP_DRDYNVC_INPUT_EVENT*)freerdp_event_new(RDP_EVENT_CLASS_DRDYNVC, RDP_EVENT_TYPE_DRDYNVC_INPUT, NULL, NULL);
	
	e->encodeTime = 0;//event[0].time;
	e->frameCount = count;
	for(int i = 0; i < count; i++)
	{
	    e->frames[i].contactCount = event[i].contactCount;
	    e->frames[i].frameOffset = event[i].time - event[0].time;
	    for(int j = 0; j < event[i].contactCount; j++)
	    {
	        float contactId     = event[i].dataSample[ANDROID_RDP_TOUCH_EVENT_NUM_SAMPLES * j + ANDROID_RDP_TOUCH_EVENT_POINTER_ID_OFFSET];
	        float x             = event[i].dataSample[ANDROID_RDP_TOUCH_EVENT_NUM_SAMPLES * j + ANDROID_RDP_TOUCH_EVENT_X_OFFSET];
	        float y             = event[i].dataSample[ANDROID_RDP_TOUCH_EVENT_NUM_SAMPLES * j + ANDROID_RDP_TOUCH_EVENT_Y_OFFSET];
	        float orientation   = event[i].dataSample[ANDROID_RDP_TOUCH_EVENT_NUM_SAMPLES * j + ANDROID_RDP_TOUCH_EVENT_ORIENTATION_OFFSET];
	        float pressure      = event[i].dataSample[ANDROID_RDP_TOUCH_EVENT_NUM_SAMPLES * j + ANDROID_RDP_TOUCH_EVENT_PRESSURE_OFFSET];
	        float major         = event[i].dataSample[ANDROID_RDP_TOUCH_EVENT_NUM_SAMPLES * j + ANDROID_RDP_TOUCH_EVENT_TOUCH_MAJOR_OFFSET];
	        float minor         = event[i].dataSample[ANDROID_RDP_TOUCH_EVENT_NUM_SAMPLES * j + ANDROID_RDP_TOUCH_EVENT_TOUCH_MINOR_OFFSET];
            	        
	        e->frames[i].contacts[j].contactId = (UINT8)contactId;
	        e->frames[i].contacts[j].fieldsPresent = CONTACT_DATA_CONTACTRECT_PRESENT | CONTACT_DATA_PRESSURE_PRESENT;
	        e->frames[i].contacts[j].x = (INT32)x;	        
	        e->frames[i].contacts[j].y = (INT32)y;
	        e->frames[i].contacts[j].contactFlags = (INT32)event[i].contactFlags;
	        e->frames[i].contacts[j].contactRectLeft = (INT16)(-major);
	        e->frames[i].contacts[j].contactRectTop = (INT16)(-minor);
	        e->frames[i].contacts[j].contactRectRight = (INT16)(major);	        
	        e->frames[i].contacts[j].contactRectBottom = (INT16)(minor);
	        e->frames[i].contacts[j].pressure = (UINT32)(pressure * 65000);
	    }
	}
	freerdp_channels_send_event(mInstance->context->channels, (RDP_EVENT*)e);    
}

BOOL AndroidFreeRDP::android_check_fds(fd_set* set)
{
	Android_Rdp_Event_t event;
	int touchEventCount = 0;
	Android_Rdp_Touch_Event_t touchEvent[MAX_TOUCH_FRAMES];	
		
	if (!FD_ISSET(mEventPipe[0], set))
		return TRUE;
	
	while (read(mEventPipe[0], &event, sizeof(event)) > 0) 
	{
	    if (event.type == ANDROID_RDP_TOUCH_EVENT)
	    {
	        memcpy(&touchEvent[touchEventCount++], &event, sizeof(Android_Rdp_Event_t));
	        if (touchEventCount >= MAX_TOUCH_FRAMES)
	        {
	            android_process_touch_event(touchEvent, touchEventCount);
	            touchEventCount = 0;
	        }
	    } else {
	        if (touchEventCount > 0)
	        {
	            android_process_touch_event(touchEvent, touchEventCount);
	            touchEventCount = 0;
	        }
	        if (event.type == ANDROID_RDP_MOUSE_EVENT)
	        {
	            android_process_mouse_event((Android_Rdp_Mouse_Event_t*)&event);
	        } else if (event.type == ANDROID_RDP_KEY_EVENT)
	        {
	            android_process_key_event((Android_Rdp_Key_Event_t*)&event);
	        }
	    }
	}
    if (touchEventCount > 0)
	{
	    android_process_touch_event(touchEvent, touchEventCount);
	    touchEventCount = 0;
	}
	        
	return TRUE;
}

int AndroidFreeRDP::androidfreerdp_run()
{
	int i;
	int fds;
	int max_fds;
	int rcount;
	int wcount;
	void* rfds[32];
	void* wfds[32];
	fd_set rfds_set;
	fd_set wfds_set;
	AndroidContext* context;
	rdpChannels* channels;

	memset(rfds, 0, sizeof(rfds));
	memset(wfds, 0, sizeof(wfds));
	
	if (!freerdp_connect(mInstance)) {
		mStopped = TRUE;
		mRunning = FALSE;
		if (mListener)
			mListener->disconnected();
		return 0;
	}
	if (mListener)
		mListener->connected();

	freerdp_start_rdp_updates_thread(mInstance);
	context = (AndroidContext*) mInstance->context;
	channels = mInstance->context->channels;

	while (!mStopped && !freerdp_shall_disconnect(mInstance)) {
		rcount = 0;
		wcount = 0;

		if (freerdp_get_fds(mInstance, rfds, &rcount, wfds, &wcount) != TRUE) {
			LOGE("Failed to get FreeRDP file descriptor\n");
			break;
		}
		if (freerdp_channels_get_fds(channels, mInstance, rfds, &rcount, wfds, &wcount) != TRUE) {
			LOGE("Failed to get channel manager file descriptor\n");
			break;
		}
		if (android_get_fds(rfds, &rcount, wfds, &wcount) != TRUE) {
			LOGE("Failed to get dfreerdp file descriptor\n");
			break;
		}

		max_fds = 0;
		FD_ZERO(&rfds_set);
		FD_ZERO(&wfds_set);

		for (i = 0; i < rcount; i++) {
			fds = (int)(long)(rfds[i]);

			if (fds > max_fds)
				max_fds = fds;

			FD_SET(fds, &rfds_set);
		}

		if (max_fds == 0) {
			continue;
		}

		if (select(max_fds + 1, &rfds_set, &wfds_set, NULL, NULL) == -1) {
			/* these are not really errors */
			if (!((errno == EAGAIN) ||
				(errno == EWOULDBLOCK) ||
				(errno == EINPROGRESS) ||
				(errno == EINTR))) /* signal occurred */ {
				LOGE("androidfreerdp_run: select failed\n");
				break;
			}
		}

		if (freerdp_check_fds(mInstance) != TRUE) {
			LOGE("Failed to check FreeRDP file descriptor\n");
			break;
		}
		if (android_check_fds(&rfds_set) != TRUE) {
			LOGE("Failed to check androidfreerdp file descriptor\n");
			break;
		}
		if (freerdp_channels_check_fds(channels, mInstance) != TRUE) {
			LOGE("Failed to check channel manager file descriptor\n");
			break;
		}
		android_process_channel_event(channels, mInstance);
	}

	freerdp_stop_rdp_updates_thread(mInstance);
	gdi_free(mInstance);
	freerdp_disconnect(mInstance);
	freerdp_channels_close(channels, mInstance);    
	mStopped = TRUE;
	mRunning = FALSE;
	if (mListener)
		mListener->disconnected();	

	return 0;
}

void AndroidFreeRDP::parseArgs(int argc, char* argv[])
{
	if (mRunning)
		return;
    
    mInstance->context->argc = argc;
	mInstance->context->argv = argv;
	freerdp_client_parse_command_line_arguments(argc, argv, mInstance->settings);
	freerdp_client_load_addins(mInstance->context->channels, mInstance->settings);
}

void* AndroidFreeRDP::threadEntry(void* me)
{	
	JavaVM *vm = getJavaVM();;
    JNIEnv *env;
    vm->AttachCurrentThread(&env, NULL);

    //androidSetThreadPriority(0, ANDROID_PRIORITY_FOREGROUND);

#ifdef ENABLE_LIBRARY_PROFILER
    monstartup("librdpjni.so");
#endif

    static_cast<AndroidFreeRDP *>(me)->androidfreerdp_run();

#ifdef ENABLE_LIBRARY_PROFILER	
	moncleanup();
#endif

    vm->DetachCurrentThread();

    return NULL;
}

void AndroidFreeRDP::connect()
{	
	if (mRunning)
		return;
		
	mRunning = TRUE;
	mStopped = FALSE;
	
	pthread_attr_t attr;
	
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);			
	pthread_create(&mThread, &attr, AndroidFreeRDP::threadEntry, this);
	
	pthread_attr_destroy(&attr);
}

void AndroidFreeRDP::android_post_event(Android_Rdp_Event_t& event)
{
	write(mEventPipe[1], &event, sizeof(event));
}

void AndroidFreeRDP::disconnect()
{
	if (mThread != 0) {
		Android_Rdp_Event_t quitEvent;
		void *dummy;

		mStopped = TRUE;
		quitEvent.type = ANDROID_RDP_NULL_EVENT;
		android_post_event(quitEvent);
		pthread_join(mThread, &dummy);
		mThread = 0;
	}
}

void AndroidFreeRDP::sendMouseEvent(int x, int y, UINT16 device_flags)
{
	Android_Rdp_Event_t event;

	event.type = ANDROID_RDP_MOUSE_EVENT;
	event.mouse.x = x;
	event.mouse.y = y;
	event.mouse.device_flags = device_flags;
	android_post_event(event);
}

void AndroidFreeRDP::sendKeyEvent(int keycode, bool unicode, UINT16 device_flags)
{
	Android_Rdp_Event_t event;

	event.type = ANDROID_RDP_KEY_EVENT;
	event.key.keycode = keycode;
	event.key.unicode = unicode;
	event.key.device_flags = device_flags;
	android_post_event(event);
}

void AndroidFreeRDP::sendMotionEvent(int contactFlags, UINT64 time, UINT32 contactCount, float dataSample[])
{
    Android_Rdp_Touch_Event_t event;
    
    event.type = ANDROID_RDP_TOUCH_EVENT;
    event.contactFlags = contactFlags;
    event.time = time;
    event.contactCount = contactCount > MAX_TOUCH_CONTACTS ? MAX_TOUCH_CONTACTS : contactCount;
    memcpy(event.dataSample, dataSample, sizeof(float) * event.contactCount * ANDROID_RDP_TOUCH_EVENT_NUM_SAMPLES);    
    android_post_event((Android_Rdp_Event_t&)event);
}

