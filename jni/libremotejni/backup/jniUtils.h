#ifndef _JNI_UTILS_H_
#define _JNI_UTILS_H_

#include <stdlib.h>
#include <jni.h>
#include <android/log.h>

#define LOGI(...) __android_log_print(ANDROID_LOG_DEBUG, TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)
#define LOG_ASSERT(condition, ...) if(condition)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__);
#ifndef TAG
#define TAG "rdpjni"
#endif

#ifdef __cplusplus
extern "C"
{
#endif

int jniThrowException(JNIEnv* env, const char* className, const char* msg);

JNIEnv* getJNIEnv();

JavaVM* getJavaVM();

int jniRegisterNativeMethods(JNIEnv* env,
                             const char* className,
                             const JNINativeMethod* gMethods,
                             int numMethods);
							 
const char* EventToString(int event);						

#ifdef __cplusplus
}
#endif

#endif /* _JNI_UTILS_H_ */
