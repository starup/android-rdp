#include <stdlib.h>
#include <android/log.h>

#include "jniUtils.h"
#include "android_freerdp.hxx"

extern int register_com_softmedia_rdp_FreeRDP(JNIEnv *env);

extern "C" {

JavaVM *gJavaVm;

#undef putchar
int	 putchar(const int c)
{
	int ret;
	ret = __android_log_print(ANDROID_LOG_ERROR, "FreeRDP", "%c", c);
	return ret;
}

int	 puts(const char *s)
{
	int ret;
	ret = __android_log_print(ANDROID_LOG_ERROR, "FreeRDP", "%s", s);
	return ret;
}

int printf(__const char *__restrict __format, ...)
{
	va_list ap;
	int ret;

    va_start(ap, __format);
	ret = __android_log_vprint(ANDROID_LOG_ERROR, "FreeRDP", __format, ap);
	va_end(ap);
	
	return ret;    
}
}

/*
 * Throw an exception with the specified class and an optional message.
 */
int jniThrowException(JNIEnv* env, const char* className, const char* msg) {
    jclass exceptionClass = env->FindClass(className);
    if (exceptionClass == NULL) {
        LOGE("Unable to find exception class %s", className);
        return -1;
    }

    if (env->ThrowNew(exceptionClass, msg) != JNI_OK) {
        LOGE("Failed throwing '%s' '%s'", className, msg);
    }
    return 0;
}

JavaVM* getJavaVM() {
    return gJavaVm;
}

JNIEnv* getJNIEnv() {
    JNIEnv* env = NULL;
    if (gJavaVm->GetEnv((void**) &env, JNI_VERSION_1_4) != JNI_OK) {
    	LOGE("Failed to obtain JNIEnv");
    	return NULL;
    }
    return env;
}

/*
 * Register native JNI-callable methods.
 *
 * "className" looks like "java/lang/String".
 */
int jniRegisterNativeMethods(JNIEnv* env,
                             const char* className,
                             const JNINativeMethod* gMethods,
                             int numMethods)
{
    jclass clazz;

    LOGI("Registering %s natives\n", className);
    clazz = env->FindClass(className);
    if (clazz == NULL) {
        LOGE("Native registration unable to find class '%s'\n", className);
        return -1;
    }
    if (env->RegisterNatives(clazz, gMethods, numMethods) < 0) {
        LOGE("RegisterNatives failed for '%s'\n", className);
        return -1;
    }
    return 0;
}

extern "C" jint JNI_OnLoad2(JavaVM* vm, void* reserved);

__attribute__ ((visibility("default")))  jint JNI_OnLoad(JavaVM* vm, void* reserved) {
    JNIEnv* env = NULL;
    jint result = JNI_ERR;
	gJavaVm = vm;

    if (vm->GetEnv((void**) &env, JNI_VERSION_1_4) != JNI_OK) {
        LOGE("GetEnv failed!");
        return result;
    }

    LOGI("loading . . .");

    //if(register_com_softmedia_rdp_FreeRDP(env) != JNI_OK) {
    //    LOGE("can't load com_softmedia_rdp_FreeRDP");
    //    goto end;
    //}    
	JNI_OnLoad2(vm, reserved);
	
    LOGI("loaded");

    result = JNI_VERSION_1_4;

end:
    return result;
}

const char* EventToString(int event)
{
	switch(event)
	{
		case RDP_NOP:
			return "RDP_NOP";
		case RDP_CONNECTED:
			return "RDP_CONNECTED";
		case RDP_DISCONNECTED:
			return "RDP_DISCONNECTED";
		case RDP_UPDATE_SCREEN:
			return "RDP_UPDATE_SCREEN";
		case RDP_SET_DISPLAY_MODE:
			return "RDP_SET_DISPLAY_MODE";
		default:
			return "UNKNOWN";
	}
}

