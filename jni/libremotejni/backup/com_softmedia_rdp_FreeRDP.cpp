#include <android/bitmap.h>

#include "jniUtils.h"
#include "android_freerdp.hxx"


struct fields_t {
    jfieldID    context;
    jmethodID   post_event;
};
static fields_t fields;

static const char* const kClassPathName = "com/softmedia/rdp/FreeRDP";

// ----------------------------------------------------------------------------
class JNIAndroidFreeRDPListener: public AndroidFreeRDPListener
{
public:
    JNIAndroidFreeRDPListener(JNIEnv* env, jobject thiz, jobject weak_thiz);
    ~JNIAndroidFreeRDPListener();

	virtual void connected();
	virtual bool verifyCertificate(char* subject, char* issuer, char* fingerprint);
	virtual void setDisplayMode(int w, int h, int d);
    virtual void updateScreen(rdpGdi* gdi, int x, int y, int w, int h);
    virtual void disconnected();

private:
    JNIAndroidFreeRDPListener();
	void postEvent(int msg, int ext1, int ext2, int ext3, int ext4);
	
    jclass      mClass;     // Reference to MediaPlayer class
    jobject     mObject;    // Weak ref to MediaPlayer Java object to call on
};

JNIAndroidFreeRDPListener::JNIAndroidFreeRDPListener(JNIEnv* env, jobject thiz, jobject weak_thiz)
{
	// Hold onto the MediaPlayer class for use in calling the static method
    // that posts events to the application thread.
    jclass clazz = env->GetObjectClass(thiz);
    if (clazz == NULL) {
        jniThrowException(env, "java/lang/Exception", kClassPathName);
        return;
    }
    mClass = (jclass)env->NewGlobalRef(clazz);

    // We use a weak reference so the MediaPlayer object can be garbage collected.
    // The reference is only used as a proxy for callbacks.
    mObject  = env->NewGlobalRef(weak_thiz);
}

JNIAndroidFreeRDPListener::~JNIAndroidFreeRDPListener()
{
}

void JNIAndroidFreeRDPListener::connected()
{
	postEvent(RDP_CONNECTED, 0, 0, 0, 0);
}

bool JNIAndroidFreeRDPListener::verifyCertificate(char* subject, char* issuer, char* fingerprint)
{
	return true;
}

void JNIAndroidFreeRDPListener::setDisplayMode(int w, int h, int d)
{
	postEvent(RDP_SET_DISPLAY_MODE, w, h, d, 0);
}

static void MyBitBlt(UINT8 *dst, int dstStride, UINT8 *src, int srcStride, int bytesPerPixel, int x, int y, int w, int h)
{
	int lineSize = w * bytesPerPixel;

	src += y * srcStride + x * bytesPerPixel;
	dst += y * dstStride + x * bytesPerPixel;
	if (true && bytesPerPixel == 4)
	{
	    for(int i = 0; i < h; i++)
    	{
    		UINT8* dst1 = dst;
    		UINT8* src1 = src;    	    
    		for(int j = 0; j < w; j++)
    		{
    		    dst1[0] = src1[2];
    		    dst1[1] = src1[1];
    		    dst1[2] = src1[0];
    		    dst1[3] = 0xFF;
    		    dst1 += 4;
    		    src1 += 4;
    		}
    		dst += dstStride;
    		src += srcStride;
    	}
	} else {
    	for(int i = 0; i < h; i++)
    	{
    		memcpy(dst, src, lineSize);
    		dst += dstStride;
    		src += srcStride;
    	}
    }
}

void JNIAndroidFreeRDPListener::updateScreen(rdpGdi* gdi, int x, int y, int w, int h)
{
    LOGI("updateScreen()    (%d, %d, %d, %d)", x, y, w, h);
	postEvent(RDP_UPDATE_SCREEN, x, y, x + w - 1, y + h - 1);
}

void JNIAndroidFreeRDPListener::disconnected()
{
	postEvent(RDP_DISCONNECTED, 0, 0, 0, 0);
}

void JNIAndroidFreeRDPListener::postEvent(int msg, int ext1, int ext2, int ext3, int ext4)
{
    JNIEnv *env = getJNIEnv();
	LOGI("postEvent: %s(%d), {%d, %d, %d, %d}", EventToString(msg), msg, ext1, ext2, ext3, ext4);
	if (env != NULL) {
		env->PushLocalFrame(20);//thread loop in native will not release LocalReference.
		env->CallStaticVoidMethod(mClass, fields.post_event, mObject, msg, ext1, ext2, ext3, ext4, 0);
		if (env->ExceptionOccurred()) {
			env->ExceptionDescribe();
			env->ExceptionClear();
		}
		env->PopLocalFrame(NULL);
	}
	//LOGI("postEvent: exit");
}

//----------------------------------------
static AndroidFreeRDP* getAndroidFreeRDP(JNIEnv* env, jobject thiz)
{
    return (AndroidFreeRDP*)env->GetIntField(thiz, fields.context);
}

static AndroidFreeRDP* setAndroidFreeRDP(JNIEnv* env, jobject thiz, AndroidFreeRDP* rdp)
{
    AndroidFreeRDP* old = (AndroidFreeRDP*)env->GetIntField(thiz, fields.context);
    env->SetIntField(thiz, fields.context, (int)rdp);
    return old;
}

static void com_softmedia_rdp_FreeRDP_native_init(JNIEnv* env, jclass clz)
{
	LOGI("native_init");

    jclass clazz;

    freerdp_channels_global_init();
    clazz = env->FindClass("com/softmedia/rdp/FreeRDP");
    if (clazz == NULL) {
        jniThrowException(env, "java/lang/RuntimeException", "Can't find com/softmedia/rdp/FreeRDP");
        return;
    }

    fields.context = env->GetFieldID(clazz, "mNativeContext", "I");
    if (fields.context == NULL) {
        jniThrowException(env, "java/lang/RuntimeException", "Can't find FreeRDP.mNativeContext");
        return;
    }
	
    fields.post_event = env->GetStaticMethodID(clazz, "postEventFromNative",
                                                   "(Ljava/lang/Object;IIIIILjava/lang/Object;)V");
    if (fields.post_event == NULL) {
        jniThrowException(env, "java/lang/RuntimeException", "Can't find FreeRDP.postEventFromNative");
        return;
    }
}

static void com_softmedia_rdp_FreeRDP_native_setup(JNIEnv* env, jobject thiz, jobject weak_ref)
{
	LOGI("native_setup");
	
    AndroidFreeRDP* mp = new AndroidFreeRDP();
    if (mp == NULL) {
        jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
        return;
    }
    // create new listener and give it to MediaPlayer
    JNIAndroidFreeRDPListener* listener = new JNIAndroidFreeRDPListener(env, thiz, weak_ref);
    mp->setListener(listener);

    // Stow our new C++ AndroidFreeRDP in an opaque field in the Java object.
    setAndroidFreeRDP(env, thiz, mp);		
}

static void com_softmedia_rdp_FreeRDP_native_finalize(JNIEnv* env, jobject thiz)
{
	AndroidFreeRDP* mp = getAndroidFreeRDP(env, thiz);
    
    LOGI("native_finalize");
    if (mp != NULL) {
        JNIAndroidFreeRDPListener* listener = (JNIAndroidFreeRDPListener*)mp->getListener();
        // this prevents native callbacks after the object is released
        mp->setListener(NULL);
        setAndroidFreeRDP(env, thiz, NULL);
        delete mp;
    }
}

static jboolean com_softmedia_rdp_FreeRDP_native_update_bitmap(JNIEnv* env, jobject thiz, jobject bitmap, jint x1, jint y1, jint x2, jint y2)
{
    AndroidBitmapInfo  info;
	int ret;
	uint8_t*			pixels;
    AndroidFreeRDP* mp = getAndroidFreeRDP(env, thiz);
    rdpGdi* gdi = mp != NULL ? mp->getRDPInstance()->context->gdi : NULL;

    LOGI("native_update_bitmap, {%d, %d, %d, %d}", x1, y1, x2, y2);
    
    if (gdi == NULL || bitmap == NULL) {
        LOGE("gdi bitmap is null");
        return JNI_FALSE;
    }
	
	if ((ret = AndroidBitmap_getInfo(env, bitmap, &info)) < 0) {
        LOGE("AndroidBitmap_getInfo() failed ! error=%d", ret);
        return JNI_FALSE;
    }

    if ((ret = AndroidBitmap_lockPixels(env, bitmap, (void**)&pixels)) < 0) {
        LOGE("AndroidBitmap_lockPixels() failed ! error=%d", ret);
        return JNI_FALSE;
    }
	LOGE("native_update_bitmap: (%d, %d, %d) --> (%d, %d, %d)", info.width, info.height, info.format, gdi->width, gdi->height, gdi->bytesPerPixel);	
	MyBitBlt(pixels, info.stride, gdi->primary_buffer, gdi->width * gdi->bytesPerPixel,
			gdi->bytesPerPixel, x1, y1, x2 - x1 + 1, y2 - y1 + 1);
	
    AndroidBitmap_unlockPixels(env, bitmap);

    return JNI_TRUE;
}

static void com_softmedia_rdp_FreeRDP_native_parse_args(JNIEnv* env, jobject thiz, jobjectArray argv)
{
	int _argc = 0;
	char **_argv = NULL;
	AndroidFreeRDP* mp = getAndroidFreeRDP(env, thiz);
	
	if (argv == NULL || mp == NULL) {
		return;
	}
	
	_argc = env->GetArrayLength(argv);
	_argv = (char **) malloc(sizeof(char *) * _argc);
	for(int i = 0; i < _argc; i++) {
		jstring str = (jstring)env->GetObjectArrayElement(argv, i);
		if (str != NULL) {
			char* cstr = (char *)env->GetStringUTFChars(str, NULL);
			_argv[i] = strdup(cstr);
			env->ReleaseStringUTFChars(str, cstr);
		} else {
			_argv[i] = strdup("");
		}
	}
	mp->parseArgs(_argc, _argv);
#if 0	
	for(int i = 0; i < _argc; i++) {
		free(_argv[i]);
	}
	free(_argv);
#endif	
}

static void com_softmedia_rdp_FreeRDP_native_connect(JNIEnv* env, jobject thiz)
{
	AndroidFreeRDP* mp = getAndroidFreeRDP(env, thiz);
	if (mp != NULL) {
		mp->connect();
	}
}

static void com_softmedia_rdp_FreeRDP_native_disconnect(JNIEnv* env, jobject thiz)
{
	AndroidFreeRDP* mp = getAndroidFreeRDP(env, thiz);
	if (mp != NULL) {
		mp->disconnect();
	}
}

static void com_softmedia_rdp_FreeRDP_native_send_mouse_event(JNIEnv* env, jobject thiz, jint x, jint y, jint device_flags)
{
	AndroidFreeRDP* mp = getAndroidFreeRDP(env, thiz);
	if (mp != NULL) {
		mp->sendMouseEvent(x, y, (UINT16)device_flags);
	}
}

static void com_softmedia_rdp_FreeRDP_native_send_key_event(JNIEnv* env, jobject thiz, jint keycode, jboolean unicode, jint device_flags)
{
	AndroidFreeRDP* mp = getAndroidFreeRDP(env, thiz);
	if (mp != NULL) {
		mp->sendKeyEvent(keycode, unicode, (UINT16)device_flags);
	}
}

static void com_softmedia_rdp_FreeRDP_native_send_motion_event(JNIEnv* env, jobject thiz, jint contactFlags, jlong time, jfloatArray dataSample)
{
    AndroidFreeRDP* mp = getAndroidFreeRDP(env, thiz);
	if (mp != NULL) {
	    jint contactCount = env->GetArrayLength(dataSample) / ANDROID_RDP_TOUCH_EVENT_NUM_SAMPLES;
	    jfloat* cDataSample = env->GetFloatArrayElements(dataSample, NULL);
		mp->sendMotionEvent(contactFlags, time, contactCount, cDataSample);
		env->ReleaseFloatArrayElements(dataSample, cDataSample, JNI_ABORT);
	}	
}

static JNINativeMethod gMethods[] = {
    {"native_init",         "()V",                             (void *)com_softmedia_rdp_FreeRDP_native_init},
    {"native_setup",        "(Ljava/lang/Object;)V",           (void *)com_softmedia_rdp_FreeRDP_native_setup},
    {"native_finalize",     "()V",                             (void *)com_softmedia_rdp_FreeRDP_native_finalize},
    {"native_update_bitmap",     "(Landroid/graphics/Bitmap;IIII)Z",                             (void *)com_softmedia_rdp_FreeRDP_native_update_bitmap},
	
	{"native_parse_args",     "([Ljava/lang/String;)V",        (void *)com_softmedia_rdp_FreeRDP_native_parse_args},
	{"native_connect",     "()V",                              (void *)com_softmedia_rdp_FreeRDP_native_connect},
	{"native_disconnect",     "()V",                           (void *)com_softmedia_rdp_FreeRDP_native_disconnect},
	{"native_send_mouse_event",     "(III)V",                  (void *)com_softmedia_rdp_FreeRDP_native_send_mouse_event},
	{"native_send_key_event",     "(IZI)V",                    (void *)com_softmedia_rdp_FreeRDP_native_send_key_event},
	{"native_send_motion_event",     "(IJ[F)V",                    (void *)com_softmedia_rdp_FreeRDP_native_send_motion_event},
};

int register_com_softmedia_rdp_FreeRDP(JNIEnv* env) {
	return jniRegisterNativeMethods(env, kClassPathName, gMethods, sizeof(gMethods) / sizeof(gMethods[0]));        
}
