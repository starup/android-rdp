#include <stdlib.h>

#include "android_jni_utils.h"

extern int register_com_softmedia_remote_rdp_LibFreeRDP(JNIEnv *env);
extern int register_com_softmedia_remote_vnc_LibVNCClient(JNIEnv *env);

JavaVM *gJavaVm;

#ifdef WITH_DEBUG_ANDROID_JNI

#undef putchar
int	 putchar(const int c)
{
	int ret;
	ret = __android_log_print(ANDROID_LOG_ERROR, "FreeRDP", "%c", c);
	return ret;
}

int	 puts(const char *s)
{
	int ret;
	ret = __android_log_print(ANDROID_LOG_ERROR, "FreeRDP", "%s", s);
	return ret;
}

int printf(__const char *__restrict __format, ...)
{
	va_list ap;
	int ret;

    va_start(ap, __format);
	ret = __android_log_vprint(ANDROID_LOG_ERROR, "FreeRDP", __format, ap);
	va_end(ap);
	
	return ret;    
}

#endif /*WITH_DEBUG_ANDROID_JNI*/

/*
 * Throw an exception with the specified class and an optional message.
 */
int jniThrowException(JNIEnv* env, const char* className, const char* msg) {
    jclass exceptionClass = (*env)->FindClass(env, className);
    if (exceptionClass == NULL) {
        DEBUG_ANDROID("Unable to find exception class %s", className);
        return -1;
    }

    if ((*env)->ThrowNew(env, exceptionClass, msg) != JNI_OK) {
        DEBUG_ANDROID("Failed throwing '%s' '%s'", className, msg);
    }
    return 0;
}

JavaVM* getJavaVM() {
    return gJavaVm;
}

JNIEnv* getJNIEnv() {
    JNIEnv* env = NULL;
    if ((*gJavaVm)->GetEnv(gJavaVm, (void**) &env, JNI_VERSION_1_4) != JNI_OK) {
    	DEBUG_ANDROID("Failed to obtain JNIEnv");
    	return NULL;
    }
    return env;
}

/*
 * Register native JNI-callable methods.
 *
 * "className" looks like "java/lang/String".
 */
int jniRegisterNativeMethods(JNIEnv* env,
                             const char* className,
                             const JNINativeMethod* gMethods,
                             int numMethods)
{
    jclass clazz;

    DEBUG_ANDROID("Registering %s natives\n", className);
    clazz = (*env)->FindClass(env, className);
    if (clazz == NULL) {
        DEBUG_ANDROID("Native registration unable to find class '%s'\n", className);
        return -1;
    }
    if ((*env)->RegisterNatives(env, clazz, gMethods, numMethods) < 0) {
        DEBUG_ANDROID("RegisterNatives failed for '%s'\n", className);
        return -1;
    }
    return 0;
}

jstring jniNewStringUTF(JNIEnv* env, const char* in, int len)
{
	jstring out = NULL;
	jchar* unicode = NULL;
	jint result_size = 0;
	jint i;
	unsigned char* utf8 = (unsigned char*)in;

	if (!in)
	{
		return NULL;
	}
	if (len < 0)
		len = strlen(in);

	unicode = (jchar*)malloc(sizeof(jchar) * (len + 1));
	if (!unicode)
	{
		return NULL;
	}
	
	for(i = 0; i < len; i++)
	{
		unsigned char one = utf8[i];
		switch(one >> 4)
		{
			case 0x00:
			case 0x01:
			case 0x02:
			case 0x03:
			case 0x04:
			case 0x05:
			case 0x06:
			case 0x07:
				unicode[result_size++] = one;
				break;
			case 0x08:
			case 0x09:
			case 0x0a:
			case 0x0b:
			//case 0x0f:
				/*
                 * Bit pattern 10xx or 1111, which are illegal start bytes.
                 * Note: 1111 is valid for normal UTF-8, but not the
                 * modified UTF-8 used here.
                 */
				break;
			case 0x0f:
			case 0x0e:
				// Bit pattern 111x, so there are two additional bytes.				
				if (i < (len - 2))
				{
					unsigned char two = utf8[i+1];
					unsigned char three = utf8[i+2];
					if ((two & 0xc0) == 0x80 && (three & 0xc0) == 0x80)
					{
						i += 2;
						unicode[result_size++] = 
								((one & 0x0f) << 12) 
							  | ((two & 0x3f) << 6)
							  | (three & 0x3f);					
					}
				}
				break;
			case 0x0c:
			case 0x0d:
				// Bit pattern 110x, so there is one additional byte.
				if (i < (len - 1))
				{
					unsigned char two = utf8[i+1];
					if ((two & 0xc0) == 0x80)
					{
						i += 1;
						unicode[result_size++] = 
								((one & 0x1f) << 6)
						  	  | (two & 0x3f);
					}
				}
				break;				
		}
	}
	
	out = (*env)->NewString(env, unicode, result_size);
	free(unicode);

	return out;
}

__attribute__ ((visibility("default")))  jint JNI_OnLoad(JavaVM* vm, void* reserved) {
    JNIEnv* env = NULL;
    jint result = JNI_ERR;
	gJavaVm = vm;

    if ((*vm)->GetEnv(vm, (void**) &env, JNI_VERSION_1_4) != JNI_OK) {
        DEBUG_ANDROID("GetEnv failed!");
        return result;
    }

    DEBUG_ANDROID("loading . . .");

    if(register_com_softmedia_remote_rdp_LibFreeRDP(env) != JNI_OK) {
        DEBUG_ANDROID("can't load com_softmedia_remote_rdp_LibFreeRDP");
        goto end;
    }

    if(register_com_softmedia_remote_vnc_LibVNCClient(env) != JNI_OK) {
        DEBUG_ANDROID("can't load com_softmedia_remote_vnc_LibVNCServer");
        goto end;
    }
	
    DEBUG_ANDROID("loaded");

    result = JNI_VERSION_1_4;

end:
    return result;
}

