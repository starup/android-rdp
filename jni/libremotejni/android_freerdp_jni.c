/*
   FreeRDP: A Remote Desktop Protocol client.
   Android JNI Bindings and Native Code

   Copyright 2010 Marc-Andre Moreau <marcandre.moreau@gmail.com>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <locale.h>
#include "android_jni_utils.h"
#include "android_freerdp.h"

static const char* const kClassPathName = "com/softmedia/remote/rdp/LibFreeRDP";

static JNINativeMethod gMethods[] = {
    {"freerdp_new",         "()I",                             (void *)jni_freerdp_new},
    {"freerdp_free",        "(I)V",           (void *)jni_freerdp_free},
	
    {"freerdp_connect",     "(I)Z",                             (void *)jni_freerdp_connect},
    {"freerdp_disconnect",     "(I)Z",                             (void *)jni_freerdp_disconnect},
	{"freerdp_cancel_connection",     "(I)V",                             (void *)jni_freerdp_cancel_connection},
	
	{"freerdp_set_connection_info",       "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIZILjava/lang/String;)V", (void *)jni_freerdp_set_connection_info},
	{"freerdp_set_gateway_info", "(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", (void*)jni_freerdp_set_gateway_info},
	{"freerdp_set_performance_flags",     "(IZZZZZZZ)V",                              (void *)jni_freerdp_set_performance_flags},
	{"freerdp_set_audio_redirect",     "(II)V",                              (void *)jni_freerdp_set_audio_redirect},
	{"freerdp_set_clipboard_redirect",     "(IZ)V",                              (void *)jni_freerdp_set_clipboard_redirect},
	{"freerdp_set_drive_redirect",     "(ILjava/lang/String;)V",                              (void *)jni_freerdp_set_drive_redirect},
	{"freerdp_set_advanced_settings",     "(ILjava/lang/String;Ljava/lang/String;)V", (void *)jni_freerdp_set_advanced_settings},
	{"freerdp_set_data_directory",     	  "(ILjava/lang/String;)V",                  (void *)jni_freerdp_set_data_directory},
	
	
	{"freerdp_update_graphics",     "(ILandroid/graphics/Bitmap;IIII)Z",                    (void *)jni_freerdp_update_graphics},
	{"freerdp_send_cursor_event",     "(IIII)V",                    (void *)jni_freerdp_send_cursor_event},
	{"freerdp_send_key_event",     "(IIZ)V",                    (void *)jni_freerdp_send_key_event},
	{"freerdp_send_unicodekey_event",     "(II)V",                    (void *)jni_freerdp_send_unicodekey_event},
	{"freerdp_send_clipboard_data",     "(ILjava/lang/String;)V",                    (void *)jni_freerdp_send_clipboard_data},
	{"freerdp_get_version",     "()Ljava/lang/String;",                    (void *)jni_freerdp_get_version},
};

int register_com_softmedia_remote_rdp_LibFreeRDP(JNIEnv* env) {

	init_callback_environment(getJavaVM());

	setlocale(LC_ALL, "");

	freerdp_channels_global_init();
	
	return jniRegisterNativeMethods(env, kClassPathName, gMethods, sizeof(gMethods) / sizeof(gMethods[0]));        
}
