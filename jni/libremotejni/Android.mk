LOCAL_PATH := $(call my-dir)

#
## libremotejni.so
#
include $(CLEAR_VARS)
LOCAL_MODULE    := remotejni
LOCAL_SRC_FILES := \
		android_cliprdr.c \
		android_event.c \
		android_graphics.c \
		android_freerdp.c \
		android_freerdp_jni.c \
		android_jni_callback.c \
		android_jni_utils.c \
		
LOCAL_SRC_FILES += \
		android_vncclient_jni.c

LOCAL_LDLIBS := -llog -lz -ljnigraphics #-L$(LOCAL_PATH)/../libs/ -lcrypto -lssl

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH) \
    $(LOCAL_PATH)/../FreeRDP/include \
    $(LOCAL_PATH)/../FreeRDP/winpr/include
		
LOCAL_STATIC_LIBRARIES := freerdp vncclient
LOCAL_STATIC_LIBRARIES += avutil avcodec avutil
LOCAL_STATIC_LIBRARIES += crypto ssl crypto
LOCAL_STATIC_LIBRARIES += jpeg
LOCAL_STATIC_LIBRARIES += cpufeatures

include $(LOCAL_PATH)/../android-ndk-profiler-3.1/android-ndk-profiler.mk
include $(BUILD_SHARED_LIBRARY)
