#ifndef _ANDROID_JNI_UTILS_H_
#define _ANDROID_JNI_UTILS_H_

#include <stdlib.h>
#include <jni.h>
#include <android_debug.h>

#ifdef __cplusplus
extern "C"
{
#endif

int jniThrowException(JNIEnv* env, const char* className, const char* msg);

JNIEnv* getJNIEnv();

JavaVM* getJavaVM();

int jniRegisterNativeMethods(JNIEnv* env,
                             const char* className,
                             const JNINativeMethod* gMethods,
                             int numMethods);
jstring jniNewStringUTF(JNIEnv* env, const char* in, int len);							 

#ifdef __cplusplus
}
#endif

#endif /* _ANDROID_JNI_UTILS_H_ */
