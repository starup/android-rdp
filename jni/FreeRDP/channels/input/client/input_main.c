/**
 * FreeRDP: A Remote Desktop Protocol client.
 * Audio Input Redirection Virtual Channel
 *
 * Copyright 2010-2011 Vic Lee
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <winpr/memory.h>
#include <freerdp/utils/stream.h>
#include <freerdp/client/drdynvc.h>

#include "input_main.h"

#define EVENTID_SC_READY       0x0001
#define EVENTID_CS_READY       0x0002
#define EVENTID_TOUCH          0x0003
#define EVENTID_SUSPEND_TOUCH  0x0004
#define EVENTID_RESUME_TOUCH   0x0005
#define EVENTID_DISMISS_HOVERING_CONTACT          0x0007

#define READY_FLAGS_SHOW_TOUCH_VISUALS			0x00000001
#define READY_FLAGS_DISABLE_TIMESTAMP_INJECTION	0x00000002 

#define CHANNEL_NAME 	"Microsoft::Windows::RDS::Input"
#define PLUGIN_NAME		"input"

typedef struct _INPUT_LISTENER_CALLBACK INPUT_LISTENER_CALLBACK;
struct _INPUT_LISTENER_CALLBACK
{
	IWTSListenerCallback iface;

	IWTSPlugin* plugin;
	IWTSVirtualChannelManager* channel_mgr;
};

typedef struct _INPUT_CHANNEL_CALLBACK INPUT_CHANNEL_CALLBACK;
struct _INPUT_CHANNEL_CALLBACK
{
	IWTSVirtualChannelCallback iface;

	IWTSPlugin* plugin;
	IWTSVirtualChannelManager* channel_mgr;
	IWTSVirtualChannel* channel;
	
	BOOL suspend;
};

typedef struct _INPUT_PLUGIN INPUT_PLUGIN;
struct _INPUT_PLUGIN
{
	IWTSPlugin iface;
	
	INPUT_LISTENER_CALLBACK* listener_callback;
};

static const char* EventIDToString(UINT16 MessageId)
{
	static const char* EventStrings[] = {
		"EVENTID_SC_READY",
		"EVENTID_CS_READY",
		"EVENTID_TOUCH",
		"EVENTID_SUSPEND_TOUCH",
		"EVENTID_RESUME_TOUCH",
		"EVENTID_DISMISS_HOVERING_CONTACT",
		};
	if (MessageId > 0 && MessageId <= sizeof(EventStrings) / sizeof(EventStrings[0]))
		return EventStrings[MessageId - 1];
	return "UNKNOWN";
}

static int input_process_sc_ready(IWTSVirtualChannelCallback* pChannelCallback, STREAM* s)
{
	int error;
	STREAM* out;
	UINT32 Version;
	INPUT_CHANNEL_CALLBACK* callback = (INPUT_CHANNEL_CALLBACK*) pChannelCallback;

	stream_read_UINT32(s, Version);
	DEBUG_DVC("Version=%d.%d", (Version >> 16), (Version & 0xFFFF));

	out = stream_new(16);
	stream_write_UINT16(out, EVENTID_CS_READY);
	stream_write_UINT32(out, 16);
	stream_write_UINT32(out, READY_FLAGS_SHOW_TOUCH_VISUALS | READY_FLAGS_DISABLE_TIMESTAMP_INJECTION);
	stream_write_UINT32(out, Version);
	stream_write_UINT16(out, MAX_TOUCH_CONTACTS);
	//freerdp_hexdump(stream_get_head(out), stream_get_length(out));
	error = callback->channel->Write(callback->channel, stream_get_length(out), stream_get_head(out), NULL);
	stream_free(out);

	return error;
}

static int input_process_suspend_touch(IWTSVirtualChannelCallback* pChannelCallback, STREAM* s)
{
	INPUT_CHANNEL_CALLBACK* callback = (INPUT_CHANNEL_CALLBACK*) pChannelCallback;
	
	callback->suspend = TRUE;
	return 0;
}

static int input_process_resume_touch(IWTSVirtualChannelCallback* pChannelCallback, STREAM* s)
{
	INPUT_CHANNEL_CALLBACK* callback = (INPUT_CHANNEL_CALLBACK*) pChannelCallback;
	
	callback->suspend = FALSE;
	return 0;
}

static int input_on_data_received(IWTSVirtualChannelCallback* pChannelCallback, UINT32 cbSize, UINT8* pBuffer)
{
	int error;
	STREAM* s;
	UINT16 MessageId;
	UINT32 PduLength;

	s = stream_new(0);
	stream_attach(s, pBuffer, cbSize);

	stream_read_UINT16(s, MessageId);
	stream_read_UINT32(s, PduLength);

	DEBUG_DVC("MessageId=%s, PduLength=%d", EventIDToString(MessageId), PduLength);
	//freerdp_hexdump(pBuffer, cbSize);

	switch (MessageId)
	{
		case EVENTID_SC_READY:
			error = input_process_sc_ready(pChannelCallback, s);
			break;

		case EVENTID_SUSPEND_TOUCH:
			error = input_process_suspend_touch(pChannelCallback, s);
			break;

		case EVENTID_RESUME_TOUCH:
			error = input_process_resume_touch(pChannelCallback, s);
			break;

		default:
			DEBUG_WARN("unknown MessageId=0x%x", MessageId);
			error = 1;
			break;
	}

	stream_detach(s);
	stream_free(s);

	return error;
}

static int input_write_variable_UINT16(STREAM* stream, UINT16 val)
{
    int cb;
                
//    assert(val <= 0x7FFF);

    if (val <= 0x7F)
    {
        cb = 1;
        stream_write_BYTE(stream, (UINT8)(val & 0xFF));
    } else {
        cb = 2;
        stream_write_BYTE(stream, (UINT8)(val & 0xFF));
        stream_write_BYTE(stream, (UINT8)((val >> 8) | 0x80));
    }
    return cb;
}

static int input_write_variable_INT16(STREAM* stream, INT16 val)
{
    int cb;

//    assert(val <= 0x3FFF && val >= -0x3FFF);

    if (val <= 0x3F && val >= -0x3F)
    {
        cb = 1;
        stream_write_BYTE(stream, (UINT8)(val & 0x7F));
    } else {
        cb = 2;        
        stream_write_BYTE(stream, (UINT8)((val >> 8) | 0x80));        
        stream_write_BYTE(stream, (UINT8)(val & 0xFF));
    }
    return cb;
}

static int input_write_variable_UINT32(STREAM* stream, UINT32 val)
{
	int cb;

//    assert(val <= 0x3FFFFFFF);
    
	if (val <= 0x3F)
	{
		cb = 1;
		stream_write_BYTE(stream, (UINT8)(val & 0x3F));
	}
	else if (val <= 0x3FFF)
	{
		cb = 2;		
		stream_write_BYTE(stream, (UINT8)((val >> 8) | 0x40));
		stream_write_BYTE(stream, (UINT8)(val & 0xFF));
	}
	else if (val <= 0x3FFFFF)
	{
		cb = 3;		
		stream_write_BYTE(stream, (UINT8)((val >> 16) | 0x80));
		stream_write_BYTE(stream, (UINT8)((val >> 8) & 0xFF));
		stream_write_BYTE(stream, (UINT8)(val & 0xFF));
	} else
	{
	    cb = 4;
	    stream_write_BYTE(stream, (UINT8)((val >> 24) | 0xC0));
		stream_write_BYTE(stream, (UINT8)((val >> 16) & 0xFF));		
		stream_write_BYTE(stream, (UINT8)((val >> 8) & 0xFF));
		stream_write_BYTE(stream, (UINT8)(val & 0xFF));
	}
	return cb;
}

static int input_write_variable_INT32(STREAM* stream, INT32 val)
{
	int cb;

//    assert(val <= 0x1FFFFFFF && val >= -0x1FFFFFFF);
    
	if (val <= 0x1F && val >= -0x1F)
	{
		cb = 1;
		stream_write_BYTE(stream, (UINT8)(val & 0x1F));
	}
	else if (val <= 0x1FFF && val >= -0x1FFF)
	{
		cb = 2;		
		stream_write_BYTE(stream, (UINT8)((val >> 8) | 0x40));
		stream_write_BYTE(stream, (UINT8)(val & 0xFF));
	}
	else if (val <= 0x1FFFFF && val >= -0x1FFFFF)
	{
		cb = 3;
		stream_write_BYTE(stream, (UINT8)((val >> 16) | 0x80));
		stream_write_BYTE(stream, (UINT8)((val >> 8) & 0xFF));		
		stream_write_BYTE(stream, (UINT8)(val & 0xFF));
	} else
	{
	    cb = 4;
	    stream_write_BYTE(stream, (UINT8)((val >> 24) | 0xC0));
		stream_write_BYTE(stream, (UINT8)((val >> 16) & 0xFF));
		stream_write_BYTE(stream, (UINT8)((val >> 8) & 0xFF));
		stream_write_BYTE(stream, (UINT8)(val & 0xFF));
	}
	return cb;
}

static int input_write_variable_UINT64(STREAM* stream, UINT64 val)
{
	int cb;

//       assert(val <= 0x1FFFFFFFFFFFFFFFULL);
    
	if (val <= 0x1F)
	{
		cb = 1;
		stream_write_BYTE(stream, (UINT8)(val & 0x1F));
	}
	else if (val <= 0x1FFF)
	{
		cb = 2;
		stream_write_BYTE(stream, (UINT8)((val >> 8) | 0x20));
		stream_write_BYTE(stream, (UINT8)(val & 0xFF));		
	}
	else if (val <= 0x1FFFFF)
	{
		cb = 3;
		stream_write_BYTE(stream, (UINT8)((val >> 16) | 0x40));
		stream_write_BYTE(stream, (UINT8)((val >> 8) & 0xFF));		
		stream_write_BYTE(stream, (UINT8)(val & 0xFF));
	} else if (val <= 0x1FFFFFFF)
	{
	    cb = 4;	    
		stream_write_BYTE(stream, (UINT8)((val >> 24) | 0x60));
		stream_write_BYTE(stream, (UINT8)((val >> 16) & 0xFF));		
		stream_write_BYTE(stream, (UINT8)((val >> 8) & 0xFF));
		stream_write_BYTE(stream, (UINT8)(val & 0xFF));
	} else if (val <= 0x1FFFFFFFFFULL)
	{
	    cb = 5;
	    stream_write_BYTE(stream, (UINT8)((val >> 32) | 0x80));
	    stream_write_BYTE(stream, (UINT8)((val >> 24) & 0xFF));		
		stream_write_BYTE(stream, (UINT8)((val >> 16) & 0xFF));
		stream_write_BYTE(stream, (UINT8)((val >> 8) & 0xFF));
		stream_write_BYTE(stream, (UINT8)(val & 0xFF));
	} else if (val <= 0x1FFFFFFFFFFFULL)
	{
	    cb = 6;
	    stream_write_BYTE(stream, (UINT8)((val >> 40) | 0xA0));		
		stream_write_BYTE(stream, (UINT8)((val >> 32) & 0xFF));
		stream_write_BYTE(stream, (UINT8)((val >> 24) & 0xFF));			    
	    stream_write_BYTE(stream, (UINT8)((val >> 16) & 0xFF));
		stream_write_BYTE(stream, (UINT8)((val >> 8) & 0xFF));		
		stream_write_BYTE(stream, (UINT8)(val & 0xFF));		
	} else if (val <= 0x1FFFFFFFFFFFFFULL)
	{
	    cb = 7;	    
	    stream_write_BYTE(stream, (UINT8)((val >> 48) | 0xC0));	    		
		stream_write_BYTE(stream, (UINT8)((val >> 40) & 0xFF));
		stream_write_BYTE(stream, (UINT8)((val >> 32) & 0xFF));			    		
		stream_write_BYTE(stream, (UINT8)((val >> 24) & 0xFF));		
		stream_write_BYTE(stream, (UINT8)((val >> 16) & 0xFF));
		stream_write_BYTE(stream, (UINT8)((val >> 8) & 0xFF));		
        stream_write_BYTE(stream, (UINT8)(val & 0xFF));        
	} else if (val <= 0x1FFFFFFFFFFFFFFFULL)
	{
	    cb = 8;				
		stream_write_BYTE(stream, (UINT8)((val >> 56) | 0xE0));
		stream_write_BYTE(stream, (UINT8)((val >> 48) & 0xFF));				
		stream_write_BYTE(stream, (UINT8)((val >> 40) & 0xFF));
		stream_write_BYTE(stream, (UINT8)((val >> 32) & 0xFF));
		stream_write_BYTE(stream, (UINT8)((val >> 24) & 0xFF));
	    stream_write_BYTE(stream, (UINT8)((val >> 16) & 0xFF));			    
		stream_write_BYTE(stream, (UINT8)((val >> 8) & 0xFF));
		stream_write_BYTE(stream, (UINT8)(val & 0xFF));
	}
	return cb;
}

static int input_on_event(IWTSVirtualChannelCallback* pChannelCallback, RDP_EVENT* event)
{
    INPUT_CHANNEL_CALLBACK* callback = (INPUT_CHANNEL_CALLBACK*) pChannelCallback;
    RDP_DRDYNVC_INPUT_EVENT* input = (RDP_DRDYNVC_INPUT_EVENT*)event;
    int error = 0;
    UINT32 pos = 0;
    UINT32 i, j;
    STREAM* out;
    
    DEBUG_DVC("");

    if (!callback->suspend) 
    {                
        out = stream_new(sizeof(RDP_DRDYNVC_INPUT_EVENT) + 6);
	    stream_write_UINT16(out, EVENTID_TOUCH);
	    stream_write_UINT32(out, 6);
	    input_write_variable_UINT32(out, input->encodeTime);
	    input_write_variable_UINT16(out, input->frameCount);
	    for(i = 0; i < input->frameCount; i++)
	    {
	        input_write_variable_UINT16(out, input->frames[i].contactCount);
	        input_write_variable_UINT64(out, input->frames[i].frameOffset);
	        for(j = 0; j < input->frames[i].contactCount; j++)
	        {
	            stream_write_BYTE(out, input->frames[i].contacts[j].contactId);
	            input_write_variable_UINT16(out, input->frames[i].contacts[j].fieldsPresent);
	            input_write_variable_INT32(out, input->frames[i].contacts[j].x);
	            input_write_variable_INT32(out, input->frames[i].contacts[j].y);
	            input_write_variable_UINT32(out, input->frames[i].contacts[j].contactFlags);                   
	            if (input->frames[i].contacts[j].fieldsPresent & CONTACT_DATA_CONTACTRECT_PRESENT)
	            {
	                input_write_variable_INT16(out, input->frames[i].contacts[j].contactRectLeft);
	                input_write_variable_INT16(out, input->frames[i].contacts[j].contactRectTop);
	                input_write_variable_INT16(out, input->frames[i].contacts[j].contactRectRight);
	                input_write_variable_INT16(out, input->frames[i].contacts[j].contactRectBottom);
	            }
	            if (input->frames[i].contacts[j].fieldsPresent & CONTACT_DATA_ORIENTATION_PRESENT)
	            {
	                input_write_variable_UINT32(out, input->frames[i].contacts[j].orientation);
	            }
	            if (input->frames[i].contacts[j].fieldsPresent & CONTACT_DATA_PRESSURE_PRESENT)
	            {
	                input_write_variable_UINT32(out, input->frames[i].contacts[j].pressure);
	            }	            	            
	        }
	    }
	    pos = stream_get_pos(out);
	    stream_set_pos(out, 2);
	    stream_write_UINT32(out, pos);
	    stream_set_pos(out, pos);
        //freerdp_hexdump(stream_get_head(out), stream_get_length(out));
	    error = callback->channel->Write(callback->channel, stream_get_length(out), stream_get_head(out), NULL);
	    stream_free(out);
    }
    
    return error;
}

static int input_on_close(IWTSVirtualChannelCallback* pChannelCallback)
{
	INPUT_CHANNEL_CALLBACK* callback = (INPUT_CHANNEL_CALLBACK*) pChannelCallback;
	INPUT_PLUGIN* audin = (INPUT_PLUGIN*) callback->plugin;

	DEBUG_DVC("");

	free(callback);

	return 0;
}

static int input_on_new_channel_connection(IWTSListenerCallback* pListenerCallback,
	IWTSVirtualChannel* pChannel, UINT8* Data, int* pbAccept,
	IWTSVirtualChannelCallback** ppCallback)
{
    INPUT_LISTENER_CALLBACK* listener_callback = (INPUT_LISTENER_CALLBACK*) pListenerCallback;
	INPUT_CHANNEL_CALLBACK* callback;

	DEBUG_DVC("");

	callback = (INPUT_CHANNEL_CALLBACK*)malloc(sizeof(INPUT_CHANNEL_CALLBACK));
	ZeroMemory(callback, sizeof(INPUT_CHANNEL_CALLBACK));

	callback->iface.OnDataReceived = input_on_data_received;
	callback->iface.OnEvent = input_on_event;
	callback->iface.OnClose = input_on_close;
	callback->plugin = listener_callback->plugin;
	callback->channel_mgr = listener_callback->channel_mgr;
	callback->channel = pChannel;

	*ppCallback = (IWTSVirtualChannelCallback*) callback;

	return 0;
}

static int input_plugin_initialize(IWTSPlugin* pPlugin, IWTSVirtualChannelManager* pChannelMgr)
{
	INPUT_PLUGIN* input = (INPUT_PLUGIN*) pPlugin;

	DEBUG_DVC("");

	input->listener_callback = (INPUT_LISTENER_CALLBACK*)malloc(sizeof(INPUT_LISTENER_CALLBACK));
	ZeroMemory(input->listener_callback, sizeof(INPUT_LISTENER_CALLBACK));

	input->listener_callback->iface.OnNewChannelConnection = input_on_new_channel_connection;
	input->listener_callback->plugin = pPlugin;
	input->listener_callback->channel_mgr = pChannelMgr;

	return pChannelMgr->CreateListener(pChannelMgr, CHANNEL_NAME, 0,
		(IWTSListenerCallback*) input->listener_callback, NULL);
}

static int input_plugin_terminated(IWTSPlugin* pPlugin)
{
	INPUT_PLUGIN* input = (INPUT_PLUGIN*) pPlugin;

	DEBUG_DVC("");

	free(input->listener_callback);
	free(input);

	return 0;
}

static BOOL input_process_plugin_args(IWTSPlugin* pPlugin, ADDIN_ARGV* data)
{
	DEBUG_DVC("");

	return TRUE;
}

#ifdef STATIC_CHANNELS
#define DVCPluginEntry		input_DVCPluginEntry
#endif

int DVCPluginEntry(IDRDYNVC_ENTRY_POINTS* pEntryPoints)
{
	int error = 0;
	INPUT_PLUGIN* input;

	input = (INPUT_PLUGIN*) pEntryPoints->GetPlugin(pEntryPoints, PLUGIN_NAME);

	if (input == NULL)
	{
		input = (INPUT_PLUGIN*)malloc(sizeof(INPUT_PLUGIN));
		ZeroMemory(input, sizeof(INPUT_PLUGIN));

		input->iface.Initialize = input_plugin_initialize;
		input->iface.Connected = NULL;
		input->iface.Disconnected = NULL;
		input->iface.Terminated = input_plugin_terminated;
		error = pEntryPoints->RegisterPlugin(pEntryPoints, PLUGIN_NAME, (IWTSPlugin*) input);
	}

	if (error == 0)
		input_process_plugin_args((IWTSPlugin*) input, pEntryPoints->GetPluginData(pEntryPoints));

	return error;
}

