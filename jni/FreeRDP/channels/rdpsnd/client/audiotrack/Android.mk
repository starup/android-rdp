LOCAL_PATH := $(call my-dir)

#
## audiotrack.a
#

include $(CLEAR_VARS)

LOCAL_MODULE    := audiotrack
LOCAL_CFLAGS := -DANDROID -DHAVE_CONFIG_H

LOCAL_SRC_FILES := \
		audiotrack.cpp \
		rdpsnd_audiotrack.cpp

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)	\
	$(LOCAL_PATH)/../	\
	$(LOCAL_PATH)/../../../	\
    $(LOCAL_PATH)/../../../include
	
include $(BUILD_STATIC_LIBRARY)

